FROM ubuntu:23.10 as builder
ENV DEBIAN_FRONTEND=noninteractive
WORKDIR /build

# Some speedup tricks for shitty apt...
RUN apt-get update && apt-get install -y --no-install-recommends eatmydata \
	&& eatmydata apt-get install -y --no-install-recommends \
		ca-certificates \
		rust-all \
		pkg-config \
		libssl-dev \
		cmake \
		make \
	&& rm -fr /var/lib/apt/lists/* \
	&& eatmydata apt-get autoremove --purge -y \
		eatmydata


COPY . ./
RUN export SQLX_OFFLINE=true && unset DATABASE_URL \
    && cargo install sqlx-cli@^0.6 --no-default-features --features rustls,postgres \
    && cargo fetch --locked \
    && cargo check --jobs 16 --release --offline --locked --all-targets \
	&& cargo install --jobs 16 --path . --locked

FROM ubuntu:23.10 as server
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		curl \
		ffmpeg \
		ca-certificates \
		python3-pip \
		python3-setuptools \
		python3-wheel \
	&& rm -fr /var/lib/apt/lists/* \
    && python3 -m pip install --break-system-packages --force-reinstall https://github.com/yt-dlp/yt-dlp/archive/master.tar.gz


COPY --from=builder /root/.cargo/bin/valery-ermilov /usr/bin/
CMD ["/usr/bin/valery-ermilov"]
