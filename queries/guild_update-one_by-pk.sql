--  Copyright 2023 saber-nyan
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

-- noinspection SqlResolve @ column/"$1"
-- noinspection SqlResolve @ column/"$2"
-- noinspection SqlResolve @ column/"$3"
-- noinspection SqlResolve @ column/"$4"
-- noinspection SqlResolve @ column/"$5"
-- noinspection SqlResolve @ column/"$6"
UPDATE guild
SET modified_at    = NOW(),
	name           = $2,
	current_vc_id  = $3,
	owner_id       = $4,
	player_shuffle = $5,
	player_loop    = $6
WHERE guild_id = $1
RETURNING created_at, modified_at, guild_id, name, current_vc_id, owner_id, player_shuffle, player_loop;
