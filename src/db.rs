// Copyright 2023 saber-nyan
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use serenity::model::id::GuildId;
use sqlx::{query_file_as, Error, FromRow, Pool, Postgres};

#[derive(Debug, Clone, FromRow)]
pub struct Guild {
    pub guild_id: i64,

    pub name: String,

    pub owner_id: Option<i64>,
    pub current_vc_id: Option<i64>,

    pub player_loop: bool,
    pub player_shuffle: bool,

    pub created_at: time::OffsetDateTime,
    pub modified_at: time::OffsetDateTime,
}

#[derive(Debug, Clone, FromRow)]
pub struct Track {
    pub id: i64,

    pub name: String,
    pub url: String,
    pub thumbnail_url: String,
    pub duration: i32,

    pub played: bool,
    pub guild_id: i64,

    pub created_at: time::OffsetDateTime,
    pub modified_at: time::OffsetDateTime,
}

impl Guild {
    pub async fn create(db: &Pool<Postgres>, guild: Self) -> Result<Self, Error> {
        query_file_as!(
            Self,
            "queries/guild_insert.sql",
            guild.guild_id,
            guild.name,
            guild.current_vc_id,
            guild.owner_id,
            guild.player_shuffle,
            guild.player_loop,
        )
        .fetch_one(db)
        .await
    }

    pub async fn retrieve(db: &Pool<Postgres>, guild_id: GuildId) -> Result<Option<Self>, Error> {
        query_file_as!(
            Self,
            "queries/guild_select-one_by-pk.sql",
            guild_id.0 as i64
        )
        .fetch_optional(db)
        .await
    }

    pub async fn update(&mut self, db: &Pool<Postgres>) -> Result<Self, Error> {
        let response = query_file_as!(
            Self,
            "queries/guild_update-one_by-pk.sql",
            self.guild_id,
            self.name,
            self.current_vc_id,
            self.owner_id,
            self.player_shuffle,
            self.player_loop,
        )
        .fetch_one(db)
        .await;

        match response {
            Ok(r) => {
                *self = r.clone();
                Ok(r)
            }
            Err(e) => Err(e),
        }
    }
}

impl Track {
    pub async fn retrieve_by_guild_id(
        db: &Pool<Postgres>,
        guild_id: GuildId,
    ) -> Result<Vec<Self>, Error> {
        query_file_as!(
            Self,
            "queries/track_select-many_by-guild-id.sql",
            guild_id.0 as i64
        )
        .fetch_all(db)
        .await
    }
}
