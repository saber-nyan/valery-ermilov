// Copyright 2023 saber-nyan
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate core;

use std::env;
use std::env::VarError;
use std::num::ParseIntError;
use std::time::Duration;

use log::{error, warn, LevelFilter, SetLoggerError};
use reqwest::Client as ReqwestClient;
use serenity::framework::StandardFramework;
use serenity::model::id::UserId;
use serenity::prelude::{GatewayIntents, SerenityError, TypeMapKey};
use serenity::utils::token::InvalidToken;
use serenity::utils::validate_token;
use serenity::Client;
use simple_logger::SimpleLogger;
use songbird::SerenityInit;
use sqlx::migrate::MigrateError;
use sqlx::postgres::PgPoolOptions;
use sqlx::{migrate, Connection, Error as DbErr, PgPool};
use thiserror::Error;
use time::macros::format_description;

use crate::cmds::{HELP, MAIN_GROUP};
use crate::handlers::{after, all_message_hook, before, error_hook, unrecognised_command, Handler};
use crate::player::PLAYER_GROUP;
use crate::utils::DurationExt;

mod build;
mod cmds;
mod db;
mod handlers;
mod player;
mod utils;
mod vc;

struct ReqwestContainer;
struct PgPoolContainer;

impl TypeMapKey for ReqwestContainer {
    type Value = ReqwestClient;
}

impl TypeMapKey for PgPoolContainer {
    type Value = PgPool;
}

#[derive(Error, Debug)]
enum MainError {
    #[error("mandatory env read failed: {0}")]
    VarError(#[from] VarError),

    #[error("logger initialisation failed: {0}")]
    SetLoggerError(#[from] SetLoggerError),

    #[error("invalid owner ID: {0}")]
    ParseIntError(#[from] ParseIntError),

    #[error("discord connection failed: {0}")]
    SerenityError(#[from] SerenityError),

    #[error("invalid bot token: {0}")]
    InvalidToken(#[from] InvalidToken),

    #[error("failed to connect to PostgreSQL: {0}")]
    DBError(#[from] DbErr),

    #[error("failed to migrate PostgreSQL: {0}")]
    DBMigrateError(#[from] MigrateError),
}

#[tokio::main]
async fn main() -> Result<(), MainError> {
    let debug = env::var("DEBUG")
        .and_then(|v| Ok(v == "1"))
        .unwrap_or(false);

    let level = if debug {
        LevelFilter::Debug
    } else {
        LevelFilter::Warn
    };

    SimpleLogger::new()
        .with_colors(true)
        .with_threads(true)
        .with_utc_timestamps()
        .with_timestamp_format(format_description!(
            "[year]-[month]-[day] [hour]:[minute]:[second].[subsecond]"
        ))
        .with_level(level)
        .init()?;
    warn!("Logging initialised, debug = {debug}");

    let prefix = match env::var("PREFIX") {
        Ok(v) => v + " ",
        Err(e) => {
            error!("Bot prefix is not set, please set 'PREFIX' env");
            return Err(MainError::VarError(e));
        }
    };

    warn!("Prefix is '{prefix}'");

    let db_url = match env::var("DATABASE_URL") {
        Ok(v) => v,
        Err(e) => {
            error!("DB URL is not set, please set 'DATABASE_URL' env");
            return Err(MainError::VarError(e));
        }
    };

    let pool = match PgPoolOptions::new()
        .min_connections(16)
        .max_connections(256)
        .acquire_timeout(Duration::from_secs(5))
        .idle_timeout(Duration::from_minutes(15))
        .max_lifetime(Duration::from_minutes(15))
        .connect(&*db_url)
        .await
    {
        Ok(p) => p,
        Err(e) => {
            error!("Failed to connect to DB: {e}");
            return Err(MainError::DBError(e));
        }
    };

    let err = match pool.acquire().await {
        Ok(mut c) => c.ping().await.err(),
        Err(e) => Some(e),
    };

    if let Some(err) = err {
        error!("Failed to connect to DB: {err}");
        return Err(MainError::DBError(err));
    }

    match migrate!().run(&pool).await {
        Ok(_) => {
            warn!("Done DB migration")
        }
        Err(e) => {
            error!("DB migration failed: {e}");
            return Err(MainError::DBMigrateError(e));
        }
    }

    let token = match env::var("TOKEN") {
        Ok(v) => v,
        Err(e) => {
            error!("Bot token is not set, please set 'TOKEN' env");
            return Err(MainError::VarError(e));
        }
    };

    if let Err(e) = validate_token(token.clone()) {
        warn!("Token validation failed (but it's broken): {e}")
    }

    let owner_id_str = match env::var("OWNER") {
        Ok(v) => v,
        Err(_) => {
            warn!("Owner ID is not set, please check 'OWNER' env");
            "".to_string()
        }
    };

    let owner_id = match owner_id_str.parse::<u64>() {
        Ok(v) => v,
        Err(e) => {
            if owner_id_str.is_empty() {
                0
            } else {
                return Err(MainError::ParseIntError(e));
            }
        }
    };

    let framework = StandardFramework::new()
        .before(before)
        .after(after)
        .unrecognised_command(unrecognised_command)
        .normal_message(all_message_hook)
        .on_dispatch_error(error_hook)
        .help(&HELP)
        .group(&MAIN_GROUP)
        .group(&PLAYER_GROUP)
        .configure(|c| {
            c.allow_dm(true)
                .delimiters(vec![", ", ","])
                .prefix(prefix)
                .ignore_webhooks(true)
                .ignore_bots(false);
            if !owner_id_str.is_empty() {
                c.owners(vec![UserId(owner_id)].into_iter().collect());
            }
            c
        });

    let mut client = Client::builder(
        token,
        GatewayIntents::non_privileged() | GatewayIntents::MESSAGE_CONTENT,
    )
    .event_handler(Handler)
    .framework(framework)
    .register_songbird()
    .await?;

    client.cache_and_http.cache.set_max_messages(256);

    {
        let mut data = client.data.write().await;
        let reqwest_client = ReqwestClient::builder()
            .user_agent("Valery Ermilov ( https://gitlab.com/saber-nyan/valery-ermilov )")
            .build()
            .expect("Reqwest library failed!");

        data.insert::<ReqwestContainer>(reqwest_client);
        data.insert::<PgPoolContainer>(pool);
    }

    tokio::spawn(async move {
        let _ = client
            .start()
            .await
            .map_err(|why| error!("Client ended: {:?}", why));
    });

    tokio::signal::ctrl_c()
        .await
        .expect("failed to set signal handler");

    warn!("Received Ctrl-C, shutting down");
    Ok(())
}
