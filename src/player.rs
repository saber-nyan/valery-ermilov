// Copyright 2023 saber-nyan
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use serenity::client::Context;
use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::{Args, CommandResult};
use serenity::model::channel::Message;

#[group]
#[commands(play, playlist, stop, shuffle, repeat, skip)]
#[description("**Плеер**")]
#[only_in(guilds)]
struct Player;

#[command("сыграйте")]
#[description = "Попросить Ермилова сыграть трек"]
#[usage = "<что сыграть: всё, что может yt-dlp>"]
#[example = "https://www.youtube.com/watch?v=k2XzrnxO69E"]
async fn play(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    Ok(())
}

#[command("что слушаем")]
#[description = "Вывести плейлист"]
async fn playlist(ctx: &Context, msg: &Message) -> CommandResult {
    Ok(())
}

#[command("заебал")]
#[description = "Очистить плейлист, выйти"]
async fn stop(ctx: &Context, msg: &Message) -> CommandResult {
    Ok(())
}

#[command("перемешайте")]
#[description = "Переключить перемешивание плейлиста"]
#[usage = "1 или 0"]
#[example = "1"]
async fn shuffle(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    Ok(())
}

#[command("еще разок")]
#[description = "Настройка повтора плейлиста"]
#[usage = "track/playlist/off"]
#[example = "playlist"]
async fn repeat(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    Ok(())
}

#[command("хуйня играет")]
#[description = "Следующий трек в плейлисте"]
async fn skip(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    Ok(())
}
