// Copyright 2023 saber-nyan
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use log::{debug, error, info, warn};
use serenity::async_trait;
use serenity::client::EventHandler;
use serenity::framework::standard::macros::hook;
use serenity::framework::standard::{CommandResult, DispatchError};
use serenity::model::channel::Message;
use serenity::model::gateway::Ready;
use serenity::model::guild::Guild;
use serenity::prelude::Context;
use sqlx::{Error, Pool, Postgres};
use time::OffsetDateTime;

use crate::{db, PgPoolContainer};

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        warn!("{} is connected!", ready.user.name);
    }
}

#[hook]
pub async fn all_message_hook(_ctx: &Context, msg: &Message) {
    info!("Got message: {}", msg.content)
}

#[hook]
pub async fn error_hook(_ctx: &Context, _msg: &Message, error: DispatchError, command: &str) {
    error!("Got runtime error while executing {command}: {error:#?}")
}

#[hook]
pub async fn unrecognised_command(_ctx: &Context, _msg: &Message, unknown_command_name: &str) {
    error!("Unrecognised command: {unknown_command_name}");
}

#[hook]
pub async fn before(ctx: &Context, msg: &Message, command_name: &str) -> bool {
    debug!(
        "Got command '{}' by user '{}'",
        command_name, msg.author.name
    );

    let db = ctx
        .data
        .read()
        .await
        .get::<PgPoolContainer>()
        .expect("pool not found!")
        .clone();

    match msg.guild(&ctx.cache) {
        None => {
            warn!("Not guild, skipping");
        }
        Some(new_guild) => {
            if let Err(e) = update_guild_cache(&db, new_guild).await {
                error!("DB query failed: {e}")
            }
        }
    };

    true
}

async fn update_guild_cache(db: &Pool<Postgres>, current_guild: Guild) -> Result<db::Guild, Error> {
    match db::Guild::retrieve(db, current_guild.id).await {
        // Guild already cached in DB
        Ok(Some(mut db_guild)) => {
            db_guild.name = current_guild.name;
            db_guild.owner_id = Some(current_guild.owner_id.0 as i64);
            db_guild.update(db).await
        }
        // New guild
        Ok(None) => {
            db::Guild::create(
                db,
                db::Guild {
                    guild_id: current_guild.id.0 as i64,
                    name: current_guild.name,
                    owner_id: Some(current_guild.owner_id.0 as i64),

                    // Not used:
                    current_vc_id: None,
                    player_loop: false,
                    player_shuffle: false,
                    created_at: OffsetDateTime::UNIX_EPOCH,
                    modified_at: OffsetDateTime::UNIX_EPOCH,
                },
            )
            .await
        }
        Err(e) => {
            error!("DB query failed: {e}");
            Err(e)
        }
    }
}

#[hook]
pub async fn after(
    _ctx: &Context,
    _msg: &Message,
    command_name: &str,
    command_result: CommandResult,
) {
    match command_result {
        Ok(_) => debug!("Processed command '{command_name}'"),
        Err(why) => error!("Command '{command_name}' returned error {why:?}"),
    }
}
