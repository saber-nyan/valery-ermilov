// Copyright 2023 saber-nyan
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::collections::HashMap;

use log::error;
use serenity::client::Context;
use serenity::model::channel::Message;
use songbird::error::JoinError;
use thiserror::Error;
use url::Url;

use crate::utils::tts_url_from_config;

#[derive(Error, Debug)]
pub enum VCJoinError {
    #[error("user not in voice chat")]
    NotInVC,
    #[error("songbird failure")]
    SongbirdFail,
}

pub async fn vc_join(ctx: &Context, msg: &Message) -> Result<(), VCJoinError> {
    let (guild_id, channel_id) = {
        let guild = match msg.guild(&ctx.cache) {
            None => return Err(VCJoinError::NotInVC),
            Some(x) => x,
        };

        let channel_id = guild
            .voice_states
            .get(&msg.author.id)
            .and_then(|voice_state| voice_state.channel_id);

        (guild.id, channel_id)
    };

    let connect_to = match channel_id {
        Some(channel) => channel,
        None => {
            return Err(VCJoinError::NotInVC);
        }
    };

    let manager = match songbird::get(ctx).await {
        None => return Err(VCJoinError::SongbirdFail),
        Some(x) => x,
    }
    .clone();

    let _handler = manager.join(guild_id, connect_to).await;

    Ok(())
}

#[derive(Error, Debug)]
pub enum VCLeaveError {
    #[error("user not in voice chat")]
    NotInVC,
    #[error("songbird failure: {0}")]
    JoinError(#[from] JoinError),
    #[error("songbird failure")]
    SongbirdFail,
}

pub async fn vc_leave(ctx: &Context, msg: &Message) -> Result<(), VCLeaveError> {
    let guild = match msg.guild(&ctx.cache) {
        None => return Err(VCLeaveError::NotInVC),
        Some(x) => x,
    };
    let guild_id = guild.id;

    let manager = match songbird::get(ctx).await {
        None => return Err(VCLeaveError::SongbirdFail),
        Some(x) => x,
    }
    .clone();

    if manager.get(guild_id).is_some() {
        if let Err(e) = manager.remove(guild_id).await {
            Err(VCLeaveError::JoinError(e))
        } else {
            Ok(())
        }
    } else {
        Err(VCLeaveError::NotInVC)
    }
}

#[derive(Error, Debug)]
pub enum VCPlayError {
    #[error("invalid URL: {0}")]
    InvalidUrl(#[from] url::ParseError),
    #[error("songbird failure")]
    SongbirdFail,
    #[error("ffmpeg failure: {0}")]
    FFmpegFail(#[from] songbird::input::error::Error),
    #[error("user not in voice chat")]
    NotInVC,
    #[error("user not in guild")]
    NotInGuild,
}

pub async fn vc_play_text(
    ctx: &Context,
    msg: &Message,
    mut cfg: HashMap<String, String>,
    text: String,
) -> Result<(), VCPlayError> {
    cfg.insert("text".to_string(), text);
    let url = match tts_url_from_config(cfg) {
        Ok(u) => u,
        Err(e) => return Err(VCPlayError::InvalidUrl(e)),
    };

    vc_play_url(ctx, msg, url.to_string()).await
}

pub async fn vc_play_url(ctx: &Context, msg: &Message, url: String) -> Result<(), VCPlayError> {
    if let Err(e) = Url::parse(url.as_str()) {
        return Err(VCPlayError::InvalidUrl(e));
    }

    let guild = match msg.guild(&ctx.cache) {
        None => return Err(VCPlayError::NotInGuild),
        Some(x) => x,
    };
    let guild_id = guild.id;

    let manager = match songbird::get(ctx).await {
        None => return Err(VCPlayError::SongbirdFail),
        Some(x) => x,
    }
    .clone();

    if let Some(handler_lock) = manager.get(guild_id) {
        let mut handler = handler_lock.lock().await;

        let source = match songbird::ytdl(&url).await {
            Ok(source) => source,
            Err(e) => {
                return Err(VCPlayError::FFmpegFail(e));
            }
        };

        handler.play_source(source);
        Ok(())
    } else {
        Err(VCPlayError::NotInVC)
    }
}
