// Copyright 2023 saber-nyan
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::collections::HashMap;
use std::time::Duration;

use lazy_static::lazy_static;
use log::error;
use serenity::{model::channel::Message, Result as SerenityResult};
use url::{ParseError, Url};

lazy_static! {
    pub static ref TTS_DEFAULT_CONFIG: HashMap<String, String> = HashMap::from([
        ("format".to_string(), "mp3".to_string()),
        ("lang".to_string(), "ru_RU".to_string()),
        ("speed".to_string(), "1.0".to_string()),
        ("emotion".to_string(), "good".to_string()),
        ("speaker".to_string(), "ermilov".to_string()),
        ("text".to_string(), "".to_string()),
    ]);
}

static TTS_ERMILOV_URL: &str = "https://tts.voicetech.yandex.net/tts";

pub trait DurationExt {
    fn from_minutes(minutes: u64) -> Duration;
}

impl DurationExt for Duration {
    fn from_minutes(minutes: u64) -> Duration {
        Duration::from_secs(minutes * 60)
    }
}

pub fn check_msg(result: SerenityResult<Message>) {
    if let Err(why) = result {
        error!("Error sending message: {:?}", why);
    }
}

pub fn tts_url_from_config(cfg: HashMap<String, String>) -> Result<Url, ParseError> {
    let params = Vec::from_iter(cfg.iter());
    Url::parse_with_params(TTS_ERMILOV_URL, &params)
}
