// Copyright 2023 saber-nyan
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::collections::HashSet;
use std::default::Default;

use log::warn;
use serenity::framework::standard::help_commands::with_embeds;
use serenity::framework::standard::macros::{command, group, help};
use serenity::framework::standard::{Args, CommandGroup, CommandResult, HelpOptions};
use serenity::model::channel::Message;
use serenity::model::id::UserId;
use serenity::prelude::Context;
use serenity::utils::{content_safe, ContentSafeOptions};

use crate::utils::{check_msg, TTS_DEFAULT_CONFIG};
use crate::vc::{vc_join, vc_leave, vc_play_text, VCJoinError, VCPlayError};

#[help]
#[suggestion_text = "**Ошибка:** команда не найдена. Похожие команды: `{}`"]
#[command_not_found_text = "**Ошибка:** команда `{}` не найдена."]
#[no_help_available_text = "Описание команды недоступно."]
#[dm_only_text = "Только ЛС"]
#[guild_only_text = "Только сервера"]
#[dm_and_guild_text = "ЛС и сервера"]
#[lacking_role = "Strike"]
#[lacking_ownership = "Strike"]
#[lacking_permissions = "Strike"]
#[lacking_conditions = "Strike"]
#[wrong_channel = "Strike"]
#[embed_error_colour = "#eb9494"]
#[max_levenshtein_distance(3)]
pub async fn help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    let _ = with_embeds(context, msg, args, &help_options, groups, owners).await?;
    Ok(())
}

#[group]
#[commands(echo, say)]
#[description("**Основное**")]
struct Main;

#[command("напишите")]
#[description = "Попросить Ермилова написать что-то"]
#[usage = "<что написать>"]
#[example = "хуй"]
async fn echo(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    return match args.single_quoted::<String>() {
        Ok(x) => {
            let settings = if let Some(guild_id) = msg.guild_id {
                ContentSafeOptions::default()
                    .clean_channel(false)
                    .display_as_member_from(guild_id)
            } else {
                ContentSafeOptions::default()
                    .clean_channel(false)
                    .clean_role(false)
            };

            let content = content_safe(&ctx.cache, x, &settings, &msg.mentions);

            check_msg(msg.channel_id.say(&ctx.http, &content).await);
            Ok(())
        }
        Err(_) => {
            check_msg(msg.reply(ctx, "Че написать-то?").await);
            Ok(())
        }
    };
}

#[command("скажите")]
#[description = "Попросить Ермилова сказать что-то"]
#[usage = "<что сказать>"]
#[example = "хуй"]
#[only_in(guilds)]
async fn say(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let content = args.single_quoted::<String>().unwrap_or("".to_string());
    warn!("Saying: {content}");

    if content.is_empty() {
        check_msg(msg.reply(ctx, "Че сказать-то?").await);
        return Ok(());
    }

    if let Err(e) = vc_join(ctx, msg).await {
        match e {
            VCJoinError::NotInVC => {
                check_msg(
                    msg.reply(ctx, "А ты чё прячешься, сам сначала куда-то зайди")
                        .await,
                );
            }
            VCJoinError::SongbirdFail => {
                check_msg(msg.reply(ctx, format!("Внутренняя ошибка: {e:?}")).await);
            }
        }
        return Ok(());
    }

    let cfg = TTS_DEFAULT_CONFIG.clone();
    if let Err(e) = vc_play_text(ctx, msg, cfg, args.message().to_string()).await {
        match e {
            VCPlayError::InvalidUrl(e) => {
                check_msg(
                    msg.reply(ctx, format!("Хуйню послал, найди чё получше\n({e:?})"))
                        .await,
                );
            }
            VCPlayError::NotInVC => {
                check_msg(
                    msg.reply(ctx, "А ты чё прячешься, сам сначала куда-то зайди")
                        .await,
                );
            }
            VCPlayError::NotInGuild => {
                check_msg(msg.reply(ctx, "Сначала пригласи меня на сервер").await);
            }
            VCPlayError::FFmpegFail(e) => {
                check_msg(
                    msg.reply(ctx, format!("Внутренняя ошибка FFmpeg: {e:?}"))
                        .await,
                );
            }
            VCPlayError::SongbirdFail => {
                check_msg(msg.reply(ctx, format!("Внутренняя ошибка: {e:?}")).await);
            }
        }
        let _ = vc_leave(ctx, msg).await;
    }
    Ok(())
}
