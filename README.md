<div align="center">
<h1>
Valery Ermilov&emsp;
<a href="https://gitlab.com/saber-nyan/valery-ermilov/pipelines/master/latest" target="_blank">
<img src="https://gitlab.com/saber-nyan/valery-ermilov/badges/master/pipeline.svg" alt="CI status">
</a>
</h1>

</div>

Honorable front-line veteran, Discord-bot.

[@valery.ermilov](https://discord.com/api/oauth2/authorize?client_id=1176660023531810866&permissions=8&scope=bot)

![Valery Ermilov](res/ermilov.png)

<details>
<summary>Ermilov Jr. (test instance)</summary>

![Ermilov Jr.](res/ermilov-junior.png)

</details>
