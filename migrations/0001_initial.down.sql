--  Copyright 2023 saber-nyan
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

DROP FUNCTION IF EXISTS create_constraint_if_not_exists CASCADE;

ALTER TABLE track
	DROP CONSTRAINT IF EXISTS track_guild_id_fk_guild_guild_id CASCADE;

DROP INDEX IF EXISTS "track_guild_id" CASCADE;

DROP TABLE IF EXISTS guild CASCADE;
DROP TABLE IF EXISTS track CASCADE;
