--  Copyright 2023 saber-nyan
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

ALTER TABLE guild
	DROP COLUMN channel_id CASCADE;

ALTER TABLE guild
	ADD COLUMN current_vc_id BIGINT NULL CHECK (current_vc_id >= 0);

ALTER TABLE guild
	ADD COLUMN owner_id BIGINT NULL CHECK (owner_id >= 0);

ALTER TABLE guild
	ADD COLUMN player_loop BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE guild
	ALTER COLUMN player_loop DROP DEFAULT;

ALTER TABLE guild
	ADD COLUMN player_shuffle BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE guild
	ALTER COLUMN player_shuffle DROP DEFAULT;

ALTER TABLE track
	ADD COLUMN duration INTEGER DEFAULT 0 NOT NULL CHECK (duration >= 0);
ALTER TABLE track
	ALTER COLUMN duration DROP DEFAULT;

ALTER TABLE track
	ADD COLUMN thumbnail_url VARCHAR(32768) DEFAULT '' NOT NULL;
ALTER TABLE track
	ALTER COLUMN thumbnail_url DROP DEFAULT;
