--  Copyright 2023 saber-nyan
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

ALTER TABLE track
	DROP COLUMN thumbnail_url CASCADE;

ALTER TABLE track
	DROP COLUMN duration CASCADE;

ALTER TABLE guild
	DROP COLUMN player_shuffle CASCADE;

ALTER TABLE guild
	DROP COLUMN player_loop CASCADE;

ALTER TABLE guild
	DROP COLUMN owner_id CASCADE;

ALTER TABLE guild
	DROP COLUMN current_vc_id CASCADE;

ALTER TABLE guild
	ADD COLUMN channel_id BIGINT NULL CHECK (channel_id >= 0);
